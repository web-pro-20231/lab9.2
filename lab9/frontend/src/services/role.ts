import type { Role } from '@/types/Role'
import http from './http'

function addRole(role: Role) {
  return http.post('/Roles', role)
}

function updateRole(role: Role) {
  return http.patch(`/Roles/${role.id}`, role)
}

function delRole(role: Role) {
  return http.delete(`/Roles/${role.id}`)
}

function getRole(id: number) {
  return http.get(`/Roles/${id}`)
}

function getRoles() {
  return http.get('/Roles')
}

export default { addRole, updateRole, delRole, getRole, getRoles }
